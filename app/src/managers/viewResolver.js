import * as i from '../libs/import'

class ViewResolver {

    constructor() {
    }

    getType(){
        return "viewResolver"
    }

    getName(){
        return "viewResolver"
    }

    async initialize(app) {
        this.app = app
    }

    resolve(p){
        var view = i.views('./' + p + '.vue', true)
        if (!view) throw new Error("View not found: " + p)
        return view.default
    }
}

export default ViewResolver
