class Store {
 
    constructor() {
    }

    getType(){
        return "store"
    }

    getName(){
        return "store"
    }

    async initialize(app) {
        this.app = app
    }

    getStore(){
        return {
            state: { 
            }, 
            mutations: {
            }
        }
    }
}

export default Store
