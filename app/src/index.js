import Xixi from "xixi-core"
import XixiConnectorFirebase from "xixi-connector-firebase"
import * as i from './libs/import'
import './index.css'

const core = new Xixi()


core.use(new XixiConnectorFirebase({
    apiKey: 'AIzaSyDhJ3h0TeS90092CSdey24F9ab0P7ZI44k',
    authDomain: 'organic-5ee86.firebaseapp.com',
    projectId: 'organic-5ee86'
}))

for (var p of i.managers.keys()){
    var managerClass = i.managers(p, true).default
    var manager = new managerClass()
    core.use(manager)
}

const options = {}
core.start(options).then(function(){
    console.log('running...')
})
