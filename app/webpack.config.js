const webpack = require('webpack')
const path = require('path')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
    output: {
        //        path: resolve('../dist'),
        //publicPath: '/dist/', //publicPath tells webpack-dev-server where to serve your bundle in memory
        //        filename: '[hash].js'
    },
    module: {
        rules: [{
                test: /\.vue$/,
                //exclude: /node_modules/,
                loader: 'vue-loader'
            },
            {
                resourceQuery: /blockType=i18n/,
                type: 'javascript/auto',
                use: ['@kazupon/vue-i18n-loader', 'yaml-loader']
            },
            {
                test: /iview\/.*?js$/,
                //exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/,
                loader: ['style-loader', 'css-loader']
            },
            {
                test: /\.less$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'less-loader'
                ]
            },
            {
                test: /\.(gif|jpg|png|eot|svg|ttf|otf|woff|woff2)$/,
                loader: 'file-loader?name=[name].[ext]',
                options: {
                    name: '[name].[ext]',
                    outputPath: './'
                }
            },
            {
                test: /\.html$/,
                use: [{
                    loader: "html-loader",
                    options: {
                        minimize: true
                    }
                }]
            }
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./src/index.html",
            filename: "./index.html"
        }),
        new CopyWebpackPlugin([{
            from: 'src/external',
            to: 'external'
        }]),
        new VueLoaderPlugin()
    ],
    resolve: {
        symlinks: false,
        extensions: ['.js', '.vue'],
        alias: {
            'vue': 'vue/dist/vue.esm.js',
            '@': __dirname + '/src'
        }
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /node_modules/,
                    name: "vendor",
                    chunks: "initial",
                    enforce: true
                }
            }
        }
    }
}



